# README #

This web application, RoommateFinder, is built with Java MVC framework, JSF, with JPA for database operation. 

### During development, some configurations/components ###
 * IDE: NetBeans
 * Server: Glassfish v4.1
 * JEE version: 7.0

### Application Feature ###

* Multi-tier web based application
* Implement container security login
* Quickly find a roommate based on search criteria
* Filter options available to subscription-paid users
* Simple concurrency control
* Restful web service to export search results