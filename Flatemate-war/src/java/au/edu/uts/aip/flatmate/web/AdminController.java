package au.edu.uts.aip.flatmate.web;

import au.edu.uts.aip.flatmate.domain.Administrator;
import au.edu.uts.aip.flatmate.domain.FlatMateBean;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * admin controller for login and logout function
 * @author weiming
 */
@Named
@SessionScoped
public class AdminController extends AbstractController implements Serializable {

    @EJB
    private FlatMateBean flatMateBean;

    private Administrator admin = new Administrator();

    /**
     * get admin 
     * @return admin
     */
    public Administrator getAdmin() {
        return admin;
    }

    /**
     * set admin
     * @param admin 
     */
    public void setAdmin(Administrator admin) {
        this.admin = admin;
    }

    /**
     * for admin login
     * login and redirect to admin page
     * @return admin page if login successfully, otherwise, return null
     */
    public String login() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        try {
            if (flatMateBean.findAdmin(admin.getUsername()) == null) {
                throw new ServletException();
            }
            request.login(admin.getUsername(), admin.getPassword());   //use registerform detail to login

        } catch (ServletException e) {
            printErrorMessage("Incorrect username or password");
            return null;
        }
        return "admin?faces-redirect=true";
    }

    /**
     * for admin logout
     * logout and redirect to index page
     * @return index if logout successfully, 
     *         return null if logout unsuccessfully
     */
    public String logout() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        try {
            request.logout();
            setAdmin(new Administrator());
            return "index?faces-redirect=true";
        } catch (ServletException e) {
            printErrorMessage(e.getMessage());
            return null;
        }
    }

    /**
     * for the admin function 
     * because the full function has not been implemented
     * when admin renew the expire date, the following sentence will be printed
     */
    public void renew() {
        printInfoMessage("This full feature function has not been implemented!");
    }

}
