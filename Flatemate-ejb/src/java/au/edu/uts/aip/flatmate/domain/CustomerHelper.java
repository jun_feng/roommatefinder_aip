package au.edu.uts.aip.flatmate.domain;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class contains methods of calculating functions for customer.
 * @author junfeng
 */
public class CustomerHelper {

    /***
     * Calculate the topup points according customer's subscription level
     * @param sublvl customer subscription level
     * @return topup points
     */
    public static int calculatePointsBasedonSublvl(SubLevel sublvl) {
        int pts = -1;
        if (sublvl.equals(SubLevel.FREE)) {
            pts = 0;
        } else if (sublvl.equals(SubLevel.STANDARD)) {
            pts = 5;
        } else if (sublvl.equals(SubLevel.PREMIUM)) {
            pts = 10;
        }
        return pts;
    }

    /***
     * Calculate the expiry date customer's subscription level
     * @param sublvl customer subscription level
     * @return expiry date
     */
    public static Date calculateExpiryDate(SubLevel sublvl) {
        final int ONE_MONTH = 1;
        Date date = null;
        if (sublvl.equals(SubLevel.STANDARD) || sublvl.equals(SubLevel.PREMIUM)) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, ONE_MONTH);
            date = cal.getTime();
        } else {
            //Set the expire date to "forever" for Free subscription
            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
            String dateInString = "31-12-9999 23:59:59";
            try {
                date = sdf.parse(dateInString);
            } catch (ParseException ex) {
                Logger.getLogger(FlatMateBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return date;
    }

    /***
     * Encrypt password with SHA-256 algorithm
     * @param oldPassword plain password
     * @return encrypted password
     */
    public static String incryptePassword(String oldPassword) {
        String incryptedPw = null;
            try {
                incryptedPw = Sha.hash256(oldPassword);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(FlatMateBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        return incryptedPw;
    }

}
