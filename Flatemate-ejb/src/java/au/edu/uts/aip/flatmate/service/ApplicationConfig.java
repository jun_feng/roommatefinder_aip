package au.edu.uts.aip.flatmate.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JAX-RS configuration to host JAX-RS web services under the /rest/ path.
 * @author weiming
 */
@ApplicationPath("rest")
public class ApplicationConfig extends Application{
    
}
