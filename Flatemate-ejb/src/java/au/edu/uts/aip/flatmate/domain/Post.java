package au.edu.uts.aip.flatmate.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * a post class
 * each post has id, title, type, postcode, rent, suburb, detail, date, top date
 *               contact, email, customer, version
 * @author sunda
 */
@Entity
@XmlRootElement
public class Post implements Serializable {

    private int id;
    private String title;
    private String type;
    private String postcode;
    private int rent;
    private String suburb;
    private String detail;
    private Date date;
    private Date topDate;
    private String contact;
    private String email;
    private Customer customer;
    private int version;

    /**
     * get id - primary key, generate automatically 
     * @return id
     */
    @Id
    @GeneratedValue
    @XmlAttribute(name = "id")
    public int getId() {
        return id;
    }

    /**
     * set id
     * @param id 
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * get title
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * set title
     * @param title 
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * get type
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * set type
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * get postcode
     * @return postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * set postcode
     * @param postcode 
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * get rent
     * @return rent
     */
    public int getRent() {
        return rent;
    }

    /**
     * set rent
     * @param rent 
     */
    public void setRent(int rent) {
        this.rent = rent;
    }

    /**
     * get suburb
     * @return suburb
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     * set suburb
     * @param suburb 
     */
    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    /**
     * get detail
     * @return detail
     */
    @Lob
    public String getDetail() {
        return detail;
    }

    /**
     * set detail
     * @param detail 
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * set date using date format
     * @return date
     */
    @XmlElement(name = "Publish_Date")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDate() {
        return date;
    }

    /**
     * set date
     * @param date 
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * get top date
     * @return 
     */
    @XmlTransient
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getTopDate() {
        return topDate;
    }

    /**
     * set top date
     * @param topDate 
     */
    public void setTopDate(Date topDate) {
        this.topDate = topDate;
    }

    /**
     * get contact
     * @return contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * set contact
     * @param contact 
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * get email
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /** 
     * set email
     * @param email 
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * get customer 
     * has many to one relationship to customer
     * many posts can belong to one customer
     * @return customer
     */
    @XmlTransient
    @ManyToOne
    public Customer getCustomer() {
        return customer;
    }

    /**
     * set customer
     * @param user 
     */
    public void setCustomer(Customer user) {
        this.customer = user;
    }

    /**
     * get version
     * @return version
     */
    @XmlTransient
    @Version
    public int getVersion() {
        return version;
    }

    /**
     * set version
     * @param version 
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * toSting: make current post details to String 
     * @return post details
     */
    @Override
    public String toString() {
        String postStr = "Post properties: ";
        if (id > 0) {
            postStr += "\tid is " + id;
            postStr += "\ttitle is " + title;
            postStr += "\ttype is " + type;
            postStr += "\trent is " + rent;
            postStr += "\tsuburb is " + suburb;
            postStr += "\tdetail is " + detail;
            postStr += "\tpublishDate is " + date;
            postStr += "\ttopDate is " + topDate;
            postStr += "\tcontact is " + contact;
            postStr += "\temail is " + email;
            postStr += "\tcustomerId is " + customer.getId() + "\n";
        } else {
            postStr += "\tid == null (null post)\n";
        }
        return postStr;
    }
}
